import {
  StarshipService
} from '../../services/starshipService';
import { Dispatch, AnyAction } from "redux";
import axios from "axios";

export const StarshipActions = {
  GET_STARSHIPS_REQUEST: "GET_STARSHIPS_REQUEST",
  GET_STARSHIPS_SUCCESSFULLY: "GET_STARSHIPS_SUCCESSFULLY",
  GET_STARSHIPS_ERROR: "GET_STARSHIPS_ERROR",
  GET_FIRMS_BY_STARSHIP_REQUEST: 'GET_FIRMS_BY_STARSHIP_REQUEST',
  GET_FIRMS_BY_STARSHIP_SUCCESSFULLY: 'GET_FIRMS_BY_STARSHIP_SUCCESSFULLY',
  GET_FIRMS_BY_STARSHIP_ERROR: 'GET_FIRMS_BY_STARSHIP_ERROR',
  SEARCH_STARSHIPS_REQUEST: "SEARCH_STARSHIPS_REQUEST",
  SEARCH_STARSHIPS_SUCCESSFULLY: "SEARCH_STARSHIPS_SUCCESSFULLY",
  SEARCH_STARSHIPS_ERROR: "SEARCH_STARSHIPS_ERROR",
  GET_DETAILS_FIRM_REQUEST: "GET_DETAILS_FIRM_REQUEST",
  GET_DETAILS_FIRM_SUCCESSFULLY: "GET_DETAILS_FIRM_SUCCESSFULLY",
  GET_DETAILS_FIRM_ERROR: "GET_DETAILS_FIRM_ERROR",
};

export const fetchDetailFirm = (keyword: string) => async (dispatch: Dispatch<AnyAction>) => {
  try {
    dispatch({ type: StarshipActions.SEARCH_STARSHIPS_REQUEST });
    const dataReturn = await StarshipService.searchFirm(keyword);

    
    dispatch({
      type: StarshipActions.GET_DETAILS_FIRM_SUCCESSFULLY,
      payload: {
        film: dataReturn.results[0]
      },
    });
  } catch (error) {
    dispatch({
      type: StarshipActions.GET_DETAILS_FIRM_ERROR
    });
  }
}

export const fetchFirmByStarship = (starships: Array<any>) => async (dispatch: Dispatch<AnyAction>) => {
  try {
    dispatch({ type: StarshipActions.GET_FIRMS_BY_STARSHIP_REQUEST });

    const films: any = {};
    for (const item of starships) {
      const promiseFilm: any[] = [];
      item.films.forEach((filmUrl: string) => {
        promiseFilm.push(axios.get(filmUrl));
      })
      let filmResult = await Promise.all(promiseFilm);
      filmResult = filmResult.map((filmItem) => {
        return filmItem.data.title;
      });
      films[item.url] = filmResult;
    }
    
    dispatch({
      type: StarshipActions.GET_FIRMS_BY_STARSHIP_SUCCESSFULLY,
      payload: {
        films
      },
    });
  } catch (error) {
    dispatch({
      type: StarshipActions.GET_FIRMS_BY_STARSHIP_ERROR
    });
  }
}

export const searchStarship = (keyword: string) => async (dispatch: Dispatch<AnyAction>) => {
  try {
    dispatch({ type: StarshipActions.SEARCH_STARSHIPS_REQUEST });
    const dataReturn = await StarshipService.searchStarship(keyword);
    const mapDataReturn = dataReturn.results.map((item) => {
      return item;
    })
    dispatch(fetchFirmByStarship(mapDataReturn));
    dispatch({
      type: StarshipActions.SEARCH_STARSHIPS_SUCCESSFULLY,
      payload: {
        list: dataReturn.results,
        total: dataReturn.count
      },
    });
  } catch (err) {
    dispatch({
      type: StarshipActions.SEARCH_STARSHIPS_ERROR
    });
  }
};

export const fetchStarship = (page: Number) => async (dispatch: Dispatch<AnyAction>) => {
  try {
    dispatch({ type: StarshipActions.GET_STARSHIPS_REQUEST });
    const dataReturn = await StarshipService.getStarship(page);
    const mapDataReturn = dataReturn.results.map((item) => {
      return item;
    })
    dispatch(fetchFirmByStarship(mapDataReturn));
    dispatch({
      type: StarshipActions.GET_STARSHIPS_SUCCESSFULLY,
      payload: {
        list: dataReturn.results,
        total: dataReturn.count
      },
    });
  } catch (err) {
    dispatch({
      type: StarshipActions.GET_STARSHIPS_ERROR
    });
  }
};