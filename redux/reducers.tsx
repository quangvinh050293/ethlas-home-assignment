import { combineReducers } from "redux";
import { starshipsReducer } from "./reducers/starship.reducer";

export default combineReducers({
  starship: starshipsReducer,
});