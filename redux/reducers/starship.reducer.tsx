import { produce } from "immer";
import { AnyAction } from "redux";
import { StarshipActions } from "../actions/starship.action";

const initialListValue = {
  data: [],
  isLoadingList: false,
  total: 0,
  filmByStarship: null,
  isLoadingFilm: false,
  firmDetail: null,
  isLoadingFilmDetail: false
};

export const initialState = {
  list: { ...initialListValue },
};

export const starshipsReducer = (state = initialState, action: AnyAction) =>
  produce(state, (draft) => {
    switch (action.type) {
      case StarshipActions.GET_STARSHIPS_REQUEST:
        draft.list.isLoadingList = true;
        break;

      case StarshipActions.GET_STARSHIPS_SUCCESSFULLY: {
        const {
          payload: { list, total },
        } = action;
        draft.list.data = list;
        draft.list.total = total;
        draft.list.isLoadingList = false;
        break;
      }

      case StarshipActions.GET_STARSHIPS_ERROR:
        draft.list.isLoadingList = false;
        break;

      case StarshipActions.GET_FIRMS_BY_STARSHIP_REQUEST:
        draft.list.isLoadingFilm = true;
        break;

      case StarshipActions.GET_FIRMS_BY_STARSHIP_SUCCESSFULLY: {
        draft.list.filmByStarship = action.payload.films;
        draft.list.isLoadingFilm = false;
        break;
      }
      case StarshipActions.GET_FIRMS_BY_STARSHIP_ERROR:
        draft.list.isLoadingFilm = false;
        break;

      case StarshipActions.SEARCH_STARSHIPS_REQUEST:
        draft.list.isLoadingList = true;
        break;

      case StarshipActions.SEARCH_STARSHIPS_SUCCESSFULLY: {
        const {
          payload: { list, total },
        } = action;
        draft.list.data = list;
        draft.list.total = total;
        draft.list.isLoadingList = false;
        break;
      }

      case StarshipActions.SEARCH_STARSHIPS_ERROR:
        draft.list.isLoadingList = false;
        break;

        case StarshipActions.GET_DETAILS_FIRM_REQUEST:
          draft.list.isLoadingFilmDetail = true;
          break;
  
        case StarshipActions.GET_DETAILS_FIRM_SUCCESSFULLY: {
          const {
            payload: { film },
          } = action;
          draft.list.firmDetail = film;
          draft.list.isLoadingFilmDetail = false;
          break;
        }
  
        case StarshipActions.GET_DETAILS_FIRM_ERROR:
          draft.list.isLoadingFilmDetail = false;
          break;
      default:
        return state;
    }
  });
