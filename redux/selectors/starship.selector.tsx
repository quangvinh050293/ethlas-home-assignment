export const selectIsLoadingList = (state: any) => state.starship.list.isLoadingList;

export const selectDataList = (state: any) => state.starship.list.data;

export const selectTotal = (state: any) => state.starship.list.total;

export const selectIsLoadingFilm = (state: any) => state.starship.list.isLoadingFilm;

export const selectFilm = (state: any) => state.starship.list.filmByStarship;

export const selectIsLoadingFilmDetail = (state: any) => state.starship.list.isLoadingFilmDetail;

export const selectFilmDetail = (state: any) => state.starship.list.firmDetail;