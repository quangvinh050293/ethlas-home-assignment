import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";

const middlewareList = [];

// Redux thunk
middlewareList.push(thunk);

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(...middlewareList))
);

export default store;
