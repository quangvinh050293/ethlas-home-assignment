const path = require('path');

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')]
  },
  env: {
    API_ENDPOINT: 'https://swapi.dev/api'
  }
};

module.exports = nextConfig
