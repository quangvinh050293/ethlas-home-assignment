import http from "../utilities/httpService";
import { Starship as StarshipModel } from "../model/starship.model";
import { Firm as FirmModel} from "../model/firm.model";

interface Starship {
  count: Number;
  next: string;
  results: Array<StarshipModel>;
}

interface Firm {
  count: Number;
  next: string;
  results: Array<FirmModel>;
}

export class StarshipService {
  static async getStarship(page: Number): Promise<Starship> {
    return http.get(`/starships/?page=${page}`);
  }

  static async searchStarship(keyword: string): Promise<Starship> {
    return http.get(`/starships/?search=${keyword}`);
  }

  static async searchFirm(keyword: string): Promise<Firm> {
    return http.get(`/films/?search=${keyword}`);
  }
}
