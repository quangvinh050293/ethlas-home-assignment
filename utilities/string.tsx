export const replaceSearchValue = (item: string, searchValue: string, replaceValue: string) => {
    if (item) {
        return item.toLowerCase().replace(searchValue, replaceValue);
    }
    return "";
}