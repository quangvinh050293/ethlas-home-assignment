import React, { useEffect, useState } from "react";
import { Card, Spin, Rate } from "antd";
import { useSelector } from "react-redux";
import * as selector from "../redux/selectors/starship.selector";
import { Starship } from "../model/starship.model";

interface CardDetailProps {
  item: Starship;
  onClickCard: Function;
}

const CardDetail: React.FC<CardDetailProps> = ({ item, onClickCard }) => {
  const { isLoadingFirm, dataFirm } = useSelector((state) => ({
    isLoadingFirm: selector.selectIsLoadingFilm(state),
    dataFirm: selector.selectFilm(state),
  }));

  const [rating, setRating] = useState(0);

  const onHoverChange = (value: number) => {
    setRating(value);
    if (typeof window !== "undefined") {
      localStorage.setItem(item.url, value ? value : rating);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined" && item) {
      const rate = localStorage.getItem(item.url);
      if (rate) {
        setRating(rate);
      }
    }
  }, [item]);

  return (
    <div className="card-container" onClick={() => onClickCard(item)}>
      <Card className="card ml-4 mr-4">
        <h2>
          <span className="font-bold">Name: </span>
          <span>{item.name}</span>
        </h2>
        <h2>
          <span className="font-bold">Cost: </span>
          <span>{item.cost_in_credits}</span>
        </h2>
        <span className="font-bold">Featured Film: </span>
        <Spin spinning={isLoadingFirm}>
          {dataFirm && dataFirm[item.url] && dataFirm[item.url].length && (
            <ul className="ml-6">
              {dataFirm[item.url].map((firm) => (
                <li key={firm}>{firm}</li>
              ))}
            </ul>
          )}
        </Spin>
        <Rate onHoverChange={onHoverChange} value={rating} />
      </Card>
    </div>
  );
};

export default CardDetail;
