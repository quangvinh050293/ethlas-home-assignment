import React, { useEffect, useState, useCallback } from "react";
import { List, Input, Pagination, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchStarship,
  searchStarship,
} from "../redux/actions/starship.action";
import CardDetail from "./Card";
import ViewDetail from "./ViewDetailCard";
import * as selector from "../redux/selectors/starship.selector";
import { Starship } from "../model/starship.model";

const { Search } = Input;

const CardList: React.FC = () => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);
  const [itemCard, setItemCard] = useState<Starship>();
  const [visibleModal, setVisibleModal] = useState(false);

  const { isLoadingList, dataList, total } = useSelector((state) => ({
    isLoadingList: selector.selectIsLoadingList(state),
    dataList: selector.selectDataList(state),
    total: selector.selectTotal(state),
  }));

  useEffect(() => {
    dispatch(fetchStarship(page));
  }, [dispatch]);

  const onSearch = useCallback(
    (keyword: string) => {
      if (keyword) {
        dispatch(searchStarship(keyword));
      } else {
        setPage(1);
      }
    },
    [dispatch]
  );

  const onChangePagination = useCallback((page: number) => {
    setPage(page);
  }, []);

  const onOKModal = () => {
    setVisibleModal(false);
  };

  const onCancelModal = () => {
    setVisibleModal(false);
  };

  const onClickCard = (item: Starship) => {
    setVisibleModal(true);
    setItemCard(item);
  }

  return (
    <div className="cardList">
      <Search
        placeholder="Search starship"
        enterButton="Search"
        onSearch={onSearch}
        allowClear
        className={`search-input mb-10 w-1/2`}
      />
      <List
        grid={{
          gutter: 16,
          xs: 5,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 5,
        }}
        loading={isLoadingList}
        dataSource={dataList}
        renderItem={(item: Starship) => (
          <List.Item>
            <CardDetail item={item} onClickCard={onClickCard}/>
          </List.Item>
        )}
      />
      <Pagination current={page} onChange={onChangePagination} total={total} />

      <Modal
        title="View Detail Starship"
        okText="OK"
        cancelText="Cancel"
        width={800}
        visible={visibleModal}
        onOk={onOKModal}
        onCancel={onCancelModal}
      >
        <ViewDetail item={itemCard} />
      </Modal>
    </div>
  );
};

export default CardList;
