import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import Link from "next/link";
import { Col, Row } from "antd";
import * as selector from "../redux/selectors/starship.selector";
import { Starship } from "../model/starship.model";
import PropertyWithLabel from "./PropertyWithLabel";
import { replaceSearchValue } from "../utilities/string";

interface ViewDetailCardProps {
  item: Starship;
}

const ViewDetailCard: React.FC<ViewDetailCardProps> = ({ item }) => {
  const { name, cost_in_credits, length, max_atmosphering_speed, crew, url } =
    item;

  const { dataFirm } = useSelector((state) => ({
    dataFirm: selector.selectFilm(state),
  }));

  const properties = useMemo(() => {
    const dataProperties = [
      <PropertyWithLabel
        key="Name"
        title="Name"
        value={name}
        className="view-card-details__row"
      />,
      <PropertyWithLabel
        key="Cost"
        title="Cost"
        value={cost_in_credits}
        className="view-card-details__row"
      />,
      <Col span={24} className="device-details__row" key="description">
        <Row gutter={[8, 8]}>
          <Col span={8} className="itemTitle font-bold">
            Featured Films :
          </Col>
          <Col span={12} className="itemContent">
            {dataFirm && dataFirm[url] && dataFirm[url].length && (
              <ul>
                {dataFirm[url].map((firm: string) => (
                  <li key={firm}>
                    <Link href={`/film/${replaceSearchValue(firm, / /g, '-')}`}>
                      <a>{firm}</a>
                    </Link>
                  </li>
                ))}
              </ul>
            )}
          </Col>
        </Row>
      </Col>,
      <PropertyWithLabel
        key="Length"
        title="Length"
        value={length}
        className="view-card-details__row"
      />,
      <PropertyWithLabel
        key="Max_Atmosphering_Speed"
        title="Max Atmosphering Speed"
        value={max_atmosphering_speed}
        className="view-card-details__row"
      />,
      <PropertyWithLabel
        key="Crew"
        title="Crew"
        value={crew}
        className="view-card-details__row"
      />,
    ];
    return dataProperties;
  }, [
    name,
    cost_in_credits,
    crew,
    length,
    max_atmosphering_speed,
    url,
    dataFirm,
  ]);

  const firstColumnItemCount = Math.ceil(properties.length / 2);
  const firstColumn = properties.slice(0, firstColumnItemCount);
  const secondColumn = properties.slice(firstColumnItemCount);

  return (
    <div>
      <Row className="view-card-detail">
        <Col span={12}>{firstColumn}</Col>
        <Col span={12}>{secondColumn}</Col>
      </Row>
    </div>
  );
};

export default ViewDetailCard;
