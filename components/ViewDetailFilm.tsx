import React, { useMemo, useEffect } from "react";
import { Col, Row, Spin } from "antd";
import { fetchDetailFirm } from "../redux/actions/starship.action";
import { useDispatch, useSelector } from "react-redux";
import PropertyWithLabel from "./PropertyWithLabel";
import { replaceSearchValue } from "../utilities/string";
import * as selector from "../redux/selectors/starship.selector";

interface ViewDetailFilmProps {
  id: string;
}

const ViewDetailFilm: React.FC<ViewDetailFilmProps> = ({ id }) => {
  const dispatch = useDispatch();
  const { isLoading, film } = useSelector((state) => ({
    isLoading: selector.selectIsLoadingFilmDetail(state),
    film: selector.selectFilmDetail(state),
  }));

  useEffect(() => {
    if (id) {
      const newName = replaceSearchValue(id, /-/g, " ");
      dispatch(fetchDetailFirm(newName));
    }
  }, [dispatch, id]);
  const properties = useMemo(() => {
    let dataProperties: Array<any> = [];
    if (film) {
      dataProperties = [
        <PropertyWithLabel
          key="Title"
          title="Title"
          value={film.title}
          className="view-card-details__row"
        />,
        <PropertyWithLabel
          key="episode_id"
          title="Episode ID"
          value={film.episode_id}
          className="view-card-details__row"
        />,

        <PropertyWithLabel
          key="opening_crawl"
          title="Opening Crawl"
          value={film.opening_crawl}
          className="view-card-details__row"
        />,
        <PropertyWithLabel
          key="director"
          title="Director"
          value={film.director}
          className="view-card-details__row"
        />,
        <PropertyWithLabel
          key="Producer"
          title="Producer"
          value={film.producer}
          className="view-card-details__row"
        />,
        <PropertyWithLabel
          key="release_date"
          title="Release Date"
          value={film.release_date}
          className="view-card-details__row"
        />,
      ];
    }

    return dataProperties;
  }, [film]);

  const firstColumnItemCount = Math.ceil(properties.length / 2);
  const firstColumn = properties.slice(0, firstColumnItemCount);
  const secondColumn = properties.slice(firstColumnItemCount);

  return (
    <Spin spinning={isLoading}>
      <Row className="view-card-detail">
        <Col span={12} className="mb-6">
          {firstColumn}
        </Col>
        <Col span={12} className="mb-6">
          {secondColumn}
        </Col>
      </Row>
    </Spin>
  );
};

export default ViewDetailFilm;
