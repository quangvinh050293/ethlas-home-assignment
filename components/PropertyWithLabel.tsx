import { Col, Row } from "antd";
import React from "react";

interface PropertyWithLabelProps {
  key?: string;
  title: string;
  value: string;
  spanNumber?: string,
  className: string
}

const PropertyWithLabel: React.FC<PropertyWithLabelProps> = ({
  key,
  title,
  value,
  spanNumber,
  className,
}) => (
  <Col span={24} className={className} key={key}>
    <Row gutter={[8, 8]}>
      <Col span={8} className="itemTitle font-bold">
        {title} :
      </Col>
      <Col span={spanNumber || 12} className="itemContent">
        <div>{value}</div>
      </Col>
    </Row>
  </Col>
);

export default PropertyWithLabel;
