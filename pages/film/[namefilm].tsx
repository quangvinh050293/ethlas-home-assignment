import type { NextPage } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import ViewDetailFilm from "../../components/ViewDetailFilm";


const FilmDetail: NextPage = () => {
  const router = useRouter();
  const { namefilm } = router.query;
  
  return (
    <div className="film-container">
      <Head>
        <title>Star Wars Film</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="main">
        <h1 className="font-bold mb-6 text-xl">Star Wars Film</h1>

        <ViewDetailFilm id={namefilm} />
      </main>
    </div>
  );
};

export default FilmDetail;
